﻿/*
 * PLUGIN DATADIR
 *
 * Vietnamese language file.
 *
 * Author: Ta Xuan Truong (truongtx8 AT gmail DOT com)
 */

 theUILang.DataDir		= "Lưu vào";
 theUILang.DataDirMove		= "Di chuyển các tập tin dữ liệu";
 theUILang.datadirDlgCaption	= "Thư mục chứa dữ liệu Torrent";
 theUILang.datadirDirNotFound	= "DataDir plugin: Thư mục không hợp lệ";
 theUILang.datadirSetDirFail	= "DataDir plugin: Quá trình thực hiện thất bại";
 theUILang.datadirPHPNotFound	= "DataDir plugin: rTorrent không thể truy cập trình thông dịch PHP. Thành phần bổ sung sẽ không hoạt động";

thePlugins.get("datadir").langLoaded();