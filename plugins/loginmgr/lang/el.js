﻿/*
 * PLUGIN LoginMGR
 *
 * Greek language file.
 *
 * Author: Chris Kanatas (ckanatas@gmail.com)
 */

 theUILang.accLogin		= "Όνομα Χρήστη";
 theUILang.accPassword		= "Κωδικός Πρόσβασης";
 theUILang.accAccounts		= "Λογαριασμοί";

thePlugins.get("loginmgr").langLoaded();