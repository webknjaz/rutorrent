﻿/*
 * PLUGIN LoginMGR
 *
 * French language file.
 *
 * Author: Nicobubulle (nicobubulle@gmail.com)
 */

 theUILang.accLogin		= "Nom d'utilisateur";
 theUILang.accPassword		= "Mot de passe";
 theUILang.accAccounts		= "Comptes";

thePlugins.get("loginmgr").langLoaded();