﻿/*
 * PLUGIN LoginMGR
 *
 * Polish language file.
 *
 * Author: Dare (piczok@gmail.com)
 */

 theUILang.accLogin		= "Login";
 theUILang.accPassword		= "Hasło";
 theUILang.accAccounts		= "Konta";

thePlugins.get("loginmgr").langLoaded();