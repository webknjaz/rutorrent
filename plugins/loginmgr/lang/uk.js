﻿/*
 * PLUGIN LoginMGR
 *
 * Ukrainian language file.
 *
 * Author: Oleksandr Natalenko (pfactum@gmail.com)
 */

 theUILang.accLogin		= "Логін";
 theUILang.accPassword		= "Пароль";
 theUILang.accAccounts		= "Облікові записи";

thePlugins.get("loginmgr").langLoaded();