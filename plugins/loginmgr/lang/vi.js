﻿/*
 * PLUGIN LoginMGR
 *
 * Vietnamese language file.
 *
 * Author: Ta Xuan Truong (truongtx8 AT gmail DOT com)
 */

 theUILang.accLogin		= "Tên đăng nhập";
 theUILang.accPassword		= "Mật khẩu";
 theUILang.accAccounts		= "Tài khoản";

thePlugins.get("loginmgr").langLoaded();