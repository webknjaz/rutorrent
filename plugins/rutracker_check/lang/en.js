﻿/*
 * PLUGIN RUTRACKER_CHECK
 *
 * English language file.
 *
 * Author: 
 */

 theUILang.checkTorrent 	= "Check for Update";
 theUILang.chkHdr		= "Torrent Update Check";
 theUILang.checkedAt		= "Last Checked";
 theUILang.checkedResult	= "Result";
 theUILang.chkResults		= [
 				  "In progress",
 				  "Updated",
 				  "Up to date",
 				  "Probably deleted",
 				  "Error accessing the tracker",
 				  "Error interacting with rTorrent",
 				  "No need"
 				  ];

thePlugins.get("rutracker_check").langLoaded();