﻿/*
 * PLUGIN SCREENSHOTS
 *
 * Vietnamese language file.
 *
 * Author: Ta Xuan Truong (truongtx8 AT gmail DOT com)
 */

 theUILang.exFFMPEG		= "Screenshots";
 theUILang.exFrameWidth 	= "Frame width";
 theUILang.exFramesCount	= "Frames count";
 theUILang.exStartOffset	= "Start frame offset";
 theUILang.exBetween		= "Time between frames";
 theUILang.exSave		= "Save";
 theUILang.exSaveAll		= "Save all";
 theUILang.exScreenshot 	= "Screenshot";
 theUILang.exPlayInterval	= "Slideshow interval";
 theUILang.exImageFormat	= "Image format";

thePlugins.get("screenshots").langLoaded();